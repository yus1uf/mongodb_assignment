const MongoClient=require("mongodb").MongoClient;
const url="mongodb://localhost:27017/mydatabase";
MongoClient.connect(url,(err,client)=>{
    if (err) throw err;
    
        const db=client.db("mydatabase");
        db.collection("employee").find().toArray((err,data)=>{
            if (err) throw err;
            console.log(data)
            client.close();
        });
});
